#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.PASSWORD || !process.env.MAILBOX1 || !process.env.MAILBOX1) {
    console.log('MAILBOX1, MAILBOX2 and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const MAILBOX1 = process.env.MAILBOX1, MAILBOX2 = process.env.MAILBOX2;
    const MAIL_0_TO = MAILBOX2;
    const subject = 'Test subject ' + Math.random();
    const MAIL_0_CONTENT = 'Test content 0';
    const CONTACT_0_NAME = 'Herbert';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';

    let browser, app;
    let adminPassword = '';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(email, setIdentity) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//input[@name="Email"]'));
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[@name="Email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@name="Password"]')).sendKeys(process.env.PASSWORD);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[text()="Sign In"]')).click();
        if (setIdentity) {
            await waitForElement(By.xpath('//input[@name="Name"]'));
            await browser.sleep(3000);
            await browser.findElement(By.xpath('//input[@name="Name"]')).sendKeys('Cloudron Test Sender');
            await browser.findElement(By.xpath('//button[contains(., "Save")]')).click();
            await browser.sleep(5000);
        }

        await browser.wait(until.elementLocated(By.className('buttonComposeText')), TEST_TIMEOUT);
        await browser.sleep(5000);
    }

    async function enableAdmin() {
        execSync(`cloudron exec --app ${app.fqdn} -- sed -e 's/allow_admin_panel = .*/allow_admin_panel = On/g' -i /app/data/_data_/_default_/configs/application.ini`, EXEC_ARGS);
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/?admin');
        await browser.sleep(4000); // admin_password.txt is generated on first run
        adminPassword = execSync(`cloudron exec --app ${app.fqdn} -- cat /app/data/_data_/_default_/admin_password.txt`).toString('utf8');
        console.log('admin password:', adminPassword);
    }

    async function adminLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/?admin');
        await browser.sleep(4000);
        await waitForElement(By.xpath('//input[@name="Login"]'));
        await browser.findElement(By.xpath('//input[@name="Login"]')).sendKeys('admin');
        await waitForElement(By.xpath('//input[@name="Password"]'));
        await browser.findElement(By.xpath('//input[@name="Password"]')).sendKeys(adminPassword);
        await browser.sleep(5000);
        await waitForElement(By.xpath('//span[contains(text(), "Admin Panel")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);
        await waitForElement(By.id('top-system-dropdown-id'));
        await browser.findElement(By.id('top-system-dropdown-id')).click();
        await waitForElement(By.xpath('//*[text()="Logout"]'));
        await browser.findElement(By.xpath('//*[text()="Logout"]')).click();
        await waitForElement(By.xpath('//input[@name="Email"]'));
    }

    async function sendMail(subject) {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(5000);

        await waitForElement(By.xpath('//a[@title="New message"]'));
        await browser.findElement(By.xpath('//a[@title="New message"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//input[@list="emailaddresses-datalist"]'));
        await browser.findElement(By.xpath('//input[@list="emailaddresses-datalist"]')).sendKeys(MAIL_0_TO);
        await browser.findElement(By.xpath('//input[@name="subject"]')).sendKeys(subject);
        await browser.findElement(By.xpath('//div[@class="squire-wysiwyg"]')).sendKeys(MAIL_0_CONTENT);
        await browser.findElement(By.xpath('//span[text()="Send"]')).click();
        await browser.sleep(5000);
    }

    async function getMail(subject)  {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//*[text()="' + subject + '"]')), TEST_TIMEOUT);
    }

    async function addContact() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.className('buttonContacts'));
        await browser.findElement(By.className('buttonContacts')).click();
        await waitForElement(By.xpath('//a[contains(., "Add Contact")]'));
        await browser.findElement(By.xpath('//a[contains(., "Add Contact")]')).click();
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[contains(@placeholder, "irst name")]')).sendKeys(CONTACT_0_NAME); // First name or first name
        await browser.findElement(By.className('button-save-contact')).click();
        await browser.sleep(4000); // give some time to do the request
        await browser.findElement(By.xpath('//dialog/header/a[@class="close"]')).click();
        await browser.sleep(1000);
    }

    async function getContact() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.className('buttonContacts'));
        await browser.findElement(By.className('buttonContacts')).click();
        await waitForElement(By.xpath(`//div[contains(text(),"${CONTACT_0_NAME}")]`));
        await browser.findElement(By.xpath('//dialog/header/a[@class="close"]')).click();
        await browser.sleep(1000);
    }

    async function viewPGP() {
        await browser.get('https://' + app.fqdn + '/#/settings/openpgp');
        await (By.xpath('//button[contains(text(), "Import Key")]'));
    }

    async function checkDataAccess() {
        const response = await superagent.get('https://' + app.fqdn + '/data/VERSION').ok(() => true);
        if (response.status === 403) return;

        console.dir(response);

        throw new Error('Was able to access version');
    }

    async function checkFilters() {
        await browser.get('https://' + app.fqdn + '/#/settings/filters');
        await browser.sleep(3000); // why?
        // otherwise it says "Can't connect to server"
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//a[contains(text(), "Add a Script")]'))), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, MAILBOX1, true /* set identity */));
    it('can send mail', sendMail.bind(null, subject));
    it('check filters', checkFilters);
    it('view PGP', viewPGP);
    it('add contact', addContact);
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2, true /* set identity */));
    it('get mail', getMail.bind(null, subject));
    it('can logout', logout);

    it('can enable admin', enableAdmin);
    it('can access admin', adminLogin);

    it('cannot access data', checkDataAccess);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, EXEC_ARGS);
    });

    it('can login', login.bind(null, MAILBOX1, false /* setIdentity */));
    it('view PGP', viewPGP);
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2, false /* setIdentity */));
    it('get mail', getMail.bind(null, subject));
    it('can logout', logout);

    it('cannot access data', checkDataAccess);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, MAILBOX1, false /* setIdentity */));
    it('view PGP', viewPGP);
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2, false /* setIndentity */));
    it('get mail', getMail.bind(null, subject));
    it('check filters', checkFilters);
    it('can logout', logout);

    it('can access admin', adminLogin);

    it('cannot access data', checkDataAccess);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id eu.snappymail.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app info', getAppInfo);
    it('can login', login.bind(null, MAILBOX1, true /* setIdentity */));
    it('can send mail', sendMail.bind(null, subject));
    it('add contact', addContact);
    it('can enable admin', enableAdmin);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can login', login.bind(null, MAILBOX1, false /* setIdentity */));
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2, true /* setIdentity */));
    it('get mail', getMail.bind(null, subject));
    it('check filters', checkFilters);
    it('can logout', logout);

    it('can access admin', adminLogin);

    it('cannot access data', checkDataAccess);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});

